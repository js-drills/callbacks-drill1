const fs = require("fs");
const path = require("path");

function createRandomJSONFiles(directory, numFiles, callback) {
  fs.access(directory, fs.constants.F_OK, (err) => {
    if (err) {
      if (err.code === "ENOENT") {
        // Directory doesn't exist, create it
        fs.mkdir(directory, (err) => {
          if (err) return callback(err);
          console.log(`Directory ${directory} created`);
          createFiles();
        });
      } else {
        // Other error, pass it to the callback
        callback(err);
      }
    } else {
      // Directory exists, proceed to create files
      createFiles();
    }
  });

  function createFiles() {
    const files = Array.from({ length: numFiles }, (_, i) =>
      path.join(directory, `file${i}.json`)
    );
    let completed = 0;
    files.forEach((file) => {
      const data = JSON.stringify({ message: "Random data" });
      fs.writeFile(file, data, (err) => {
        if (err) return callback(err);
        console.log(`File ${file} created`);
        completed++;
        if (completed === numFiles) {
          callback(null, files);
        }
      });
    });
  }
}

function deleteFiles(files, callback) {
  const numToDelete = files.length - 1;
  let completed = 0;
  files.slice(0, numToDelete).forEach((file) => {
    fs.unlink(file, (err) => {
      if (err) return callback(err);
      console.log(`File ${file} deleted`);
      completed++;
      if (completed === numToDelete) {
        callback(null);
      }
    });
  });
}

module.exports = {createRandomJSONFiles,deleteFiles}


