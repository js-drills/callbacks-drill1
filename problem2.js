// problem 1 function for read file
const { error } = require("console");
let fs = require("fs");

let path = require("path");
let readingFile = (filename, callbackFunction) => {
  fs.readFile(filename, "utf-8", (error, data) => {
    if (error) {
      callbackFunction(error);
      return;
    }
    callbackFunction(null, data);
  });
};

// converting to upeercase and writing to new file
let convertingToUpperCase = (data, callbackFunction) => {
  const directory = "../problem2files";
  fs.access(directory, fs.constants.F_OK, (error) => {
    if (error) {
      if ((error = "ENOENT")) {
        fs.mkdir(directory, (error) => {
          if (error) {
            return callbackFunction(error);
          }
          console.log("directry created");
          toconvertToUpperCase();
        });
      }
    } else {
      toconvertToUpperCase();
    }
    function toconvertToUpperCase() {
      const upperCase = data.toUpperCase();
      const filename = "UpperCAseData.txt";
      const filepath = path.join(directory, filename);
      fs.writeFile(filepath, upperCase, (error) => {
        if (error) {
          return callbackFunction(error);
        }
        console.log("uppercase File Generated");
        appendNamesToFile(filepath, "filenames.txt");
        callbackFunction(null, directory, filename);
      });
    }
  });
};

/// converting to sentences of new file given

let convertingToSentences = (directory, filename, callbackFunction) => {
  fs.readFile(
    path.join(directory, filename),
    "utf-8",
    (error, upperCaseData) => {
      if (error) {
        console.log(error);
        return;
      }
      let toLowerCaseData = upperCaseData.toLowerCase();
      const sentences = toLowerCaseData.split(/[.!?]/);
      const modifiedContent = sentences.join("\n");
      fs.writeFile(
        path.join(directory, "splittedContent.txt"),
        modifiedContent,
        (error) => {
          if (error) {
            callbackFunction(err);
            return;
          }

          console.log("Split sentences file written successfully");

          callbackFunction(null, directory);
        }
      );
      appendNamesToFile(
        path.join(directory, "splittedContent.txt"),
        "filenames.txt"
      );
    }
  );
};

/// sorting the data of files
let sortingFiles = (directory, callbackFunction) => {
  let fileNamesPath = "../problem2files/filenames.txt";

  fs.readFile(fileNamesPath, "utf-8", (error, filesnames) => {
    if (error) {
      console.log(error);
      return;
    }
    let files = filesnames.split("\n");
    //   console.log();
    let sortedData = files
      .slice(0, 2)
      .map((file) => {
        return fs.readFileSync(file, "utf-8");
      })
      .sort()
      .join("\n");
    let sortedfilePath = path.join(directory, "sortedData.txt");
    fs.writeFile(sortedfilePath, sortedData, (error) => {
      if (error) {
        console.log(error);
      }
    });
    appendNamesToFile(sortedfilePath, "filenames.txt");
    console.log("Sorted files has been written succesfully");
    console.log("");
    callbackFunction(null, files);
  });
};

/// appending names to filenames.txt

function appendNamesToFile(filenameToAdd, targetFilename) {
  const filename = `../problem2files/${targetFilename}`;
  fs.readFile(filename, "utf-8", (error, data) => {
    if (error) {
      console.log(error);
      return;
    }
    if (!data.includes(filenameToAdd)) {
      fs.appendFile(filename, filenameToAdd + "\n", (err) => {
        if (err) {
          console.log(`${filenameToAdd} added to ${targetFilename}`);
          return;
        }
      });
    }
  });
}

function finalCalls() {
  const filename = "../lipsum_1.txt";
  readingFile(filename, (error, data) => {
    if (error) {
      console.log(error);
      return;
    }
    convertingToUpperCase(data, (error, directory, filename) => {
      if (error) {
        console.log(error);
        return;
      }
      convertingToSentences(directory, filename, (error, directory) => {
        if (error) {
          console.log(error);
          return;
        }
        sortingFiles(directory, (error, files) => {
          if (error) {
            console.log(error);
            return;
          }

          if (files.length === 0) {
            console.log("No files to delete.");
            return;
          }
          let deletedCount = 0;
          files.slice(0, 3).forEach((file) => {
            console.log(`${file} is deletd`);
            fs.unlink(file, (error) => {
              if (error) {
                console.error(`Error deleting file ${file}: ${error}`);
                return;
              }
              deletedCount++;
              if (deletedCount === files.length - 1) {
                console.log("All files deleted successfully.");
                // Call the additional callback here if needed
              }
            });
          });
        });
      });
      // console.log(`file is converted and stored in directory: ${directory} and file name is: ${filename}`)
    });
  });
}

module.exports = finalCalls;
