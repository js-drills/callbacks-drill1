
const functions = require('../problem1')
const createRandomJSONFiles = functions.createRandomJSONFiles
const deleteFiles = functions.deleteFiles

const directory = "./randomJsonFiles";
const numFiles = 5;

createRandomJSONFiles(directory, numFiles, (err, files) => {
  if (err) return console.error(err);
  console.log("All files created");
  deleteFiles(files, (err) => {
    if (err) return console.error(err);
    console.log("All files deleted");
  });
});